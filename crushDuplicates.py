import argparse
import os
import re
import hashlib
import threading
import queue
import sys
from PIL import Image
from PIL.ExifTags import TAGS

# Recurse through the directories, finding files that should be processed
# Files are added to the queue for processing
def findFilesToProcess(directory, filesQueue, recursive):
    p = re.compile('.*\.(jpg|jpeg)$', re.IGNORECASE)
    for childItem in os.listdir(directory):
        fullChildItem = os.path.join(directory, childItem)
        if (os.path.isfile(fullChildItem)):
            if p.match(childItem):
                filesQueue.put_nowait(fullChildItem)
        if (recursive and os.path.isdir(fullChildItem)):
            findFilesToProcess(fullChildItem, filesQueue, recursive)

# Calculate an MD5 sum based on the image pixels, ignoring other data
# This should enable detecting duplicates, even when some EXIF data has
# been modified
def calculateFileId(imageFilePath, verbose):
    if (verbose):
        print("Analyzing " + imageFilePath)

    im = Image.open(imageFilePath)
    
    m = hashlib.md5()
    
    # Get the data from the file
    m.update(im.tostring())

    # Get the number of EXIF info
    info = im._getexif()
    numExif = 0
    if (info != None):
        numExif = len(info.items())

    im.close()
    
    return (m.hexdigest(), numExif)

# Adds a processed file to the dictionary in the prop place,
# according to its digest
def addToTracking(imageFildPath, digest, numExifInfo, allDigests, realHashQueue):
    # Create the value we'll store
    duplicateInfo = DuplicateInfo(imageFildPath, numExifInfo)

    existingVal = allDigests.get(digest, None)
    if (existingVal is None):
        newVal = [ duplicateInfo ]
        allDigests[digest] = newVal
    elif (len(existingVal) == 1):
        # Just found a duplicate, so we need to calculate the hash
        # for the previously found item
        existingVal.append(duplicateInfo)
        realHashQueue.put_nowait(existingVal[0])
        realHashQueue.put_nowait(duplicateInfo)
    else:
        # We've already calcualted the hash for the previous items
        # in this duplicate, so we only need to calculate the hash
        # for this item
        existingVal.append(duplicateInfo)
        realHashQueue.put_nowait(duplicateInfo)

# Do all files in the set have the same real hash?
def allSameRealHash(duplicatesInfo):
    firstHash = duplicatesInfo[0].hash
    for duplicateInfo in duplicatesInfo:
        if (duplicateInfo.hash != firstHash):
            return False

    return True

# We need some extra info about files, but only want to calculate it
# if we actually have a duplicate. This structure allows us to have a
# place to store the info, but only calculate it if necessary.
class DuplicateInfo():
    """Stores info about a file"""
    def __init__(self, file, numExifInfo):
        self.file = file
        self.hash = 0
        self.numExifInfo = numExifInfo
        self.deleted = False

    def calculateSum(self):
        # Calculate the read MD5 sum hash, including complete file contents
        m = hashlib.md5()

        f = open(self.file, 'rb')
        while True:
            lastRead = f.read(8192)            
            m.update(lastRead)
            if (len(lastRead) < 8192):
                break

        f.close()
        
        self.hash = m.hexdigest()

# Thread worker that gets items from the file queue and calculates
# the hash for it. The hash is put into another queue for further
# processing
class HashCalculator(threading.Thread):
    """Threaded Hash Calculator"""
    def __init__(self, fileQueue, digestQueue, verbose):
        threading.Thread.__init__(self)
        self.fileQueue = fileQueue
        self.digestQueue = digestQueue
        self.verbose = verbose

    def run(self):
        while True:
            # Get the next file to process
            file = self.fileQueue.get()

            try:
                # Calculate the queue
                digestAndExifCount = calculateFileId(file, self.verbose)

                # Add the digest info to be checked for duplicates
                self.digestQueue.put_nowait((file,digestAndExifCount[0], digestAndExifCount[1]))

            except:
                print("Unable to process " + file)

            # And now done
            self.fileQueue.task_done()

# Thread worker that handles calculated caches. There must only
# be one thread worker because it accesses a dictionary in a
# thread unsafe way. But this is ok because it shouldn't take much
# work.
class DigestHandler(threading.Thread):
    """Digest Handler"""
    def __init__(self, digestQueue, realHashQueue, allDigests):
        threading.Thread.__init__(self)
        self.digestQueue = digestQueue
        self.realHashQueue = realHashQueue
        self.allDigests = allDigests

    def run(self):
        while True:
            # Get the next digest to process
            digestInfo = self.digestQueue.get()

            # Add to the dictionary
            addToTracking(digestInfo[0], digestInfo[1], digestInfo[2], self.allDigests, self.realHashQueue)

            # All done
            self.digestQueue.task_done()

# Queue worker to calculate the real hash for files. This is different from the
# pseudo-hash because it uses the full file contents, including EXIF info. This script
# only deletes files based on the true hash.
class RealHashCalculator(threading.Thread):
    """The calculator for the real hash"""
    def __init__(self, realHashQueue, verbose):
        threading.Thread.__init__(self)
        self.realHashQueue = realHashQueue
        self.verbose = verbose

    def run(self):
        while True:
            # Get the next file to process. This is a DuplicateInfo instance
            info = self.realHashQueue.get()

            try:
                # Calculate the hash
                info.calculateSum()

            except:
                print("Unable to calculate real hash for ", info.file, sys.exc_info()[0])

            self.realHashQueue.task_done()

# Gets the full path of a file with the shortest name (that we won't delete)
# Returns none if the files are not all in the same path.
def getShortestName(duplicatesInfo):
    shortestFile = duplicatesInfo[0].file
    firstDir = os.path.dirname(shortestFile)
    shortestLen = len(os.path.basename(shortestFile))
   
    for duplicateInfo in duplicatesInfo:
        if (os.path.dirname(duplicateInfo.file) != firstDir):
            return None
        currentLen = len(os.path.basename(duplicateInfo.file))
        if (currentLen < shortestLen):
            shortestFile = duplicateInfo.file
            shortestLen = currentLen

    return shortestFile

def getMaximumExifName(duplicatesInfo):
    mostExifFile = None
    mostExifCount = -1
    
    for duplicateInfo in duplicatesInfo:
        if (duplicateInfo.numExifInfo > mostExifCount):
            mostExifCount = duplicateInfo.numExifInfo
            mostExifFile = duplicateInfo.file
        elif (duplicateInfo.numExifInfo == mostExifCount):
            return None

    return mostExifFile

def deleteExcept(fileList, toKeep, verbose):
    if (toKeep != None):
        toKeep = toKeep.lower()
        # Delete the others in the set
        for duplicateInfo in fileList:
            if (duplicateInfo.file.lower() != toKeep):
                os.remove(duplicateInfo.file)
                duplicateInfo.deleted = True
                if (verbose):
                    print ("Deleting ", duplicateInfo.file)

# Delete extra copies that exist.
def autoDeleteDuplicates(allDigests, verbose):
    for fileList in allDigests.values():
        if (len(fileList) > 1):
           if (allSameRealHash(fileList)):
                # Which file has the shortest name? We'll keep that one
                deleteExcept(fileList, getShortestName(fileList), verbose)

           else:
                # Try to find by maximum EXIF info
                deleteExcept(fileList, getMaximumExifName(fileList), verbose)


# Write the XML result file containing items that are duplicates
def writeToXml(outFile, allDigests):
    outFile.write('<duplicateReport>\n')
    
    for fileList in allDigests.values():
        if (len(fileList) > 1):
            # Do all items in the set have the same hash?
            sameHash = allSameRealHash(fileList)

            outFile.write('\t<duplicateSet identical="')
            outFile.write("1" if sameHash else "0")
            outFile.write('">\n')
            for fileInfo in fileList:
                outFile.write('\t\t<duplicate path="' + fileInfo.file + '" deleted="')
                outFile.write("1" if fileInfo.deleted else "0")
                outFile.write('" numExifInfo="' + str(fileInfo.numExifInfo) + '"/>\n')
            outFile.write('\t</duplicateSet>\n')
            
    outFile.write('</duplicateReport>\n')
    outFile.flush()

# The main function that creates our threads, calls the analysis
# and prints out the results.
def main(dir, allDigests, recursive, verbose, delete, numThreads, outFile):
    # Two queues here - one to process the files that are found
    # One to handle the calculated digests. The second queue doesn't
    # need multiple workers - one is enough, but the queue gives a way
    # to avoid multiple writes to the dictionary
    fileQueue = queue.Queue()
    digestQueue = queue.Queue()
    realHashQueue = queue.Queue()

    # Create the workers to calculate hash
    for i in range(numThreads):
        t = HashCalculator(fileQueue, digestQueue, verbose)
        t.setDaemon(True)
        t.start()

    # Create the worker to handle calculated hashes
    t2 = DigestHandler(digestQueue, realHashQueue, allDigests)
    t2.setDaemon(True)
    t2.start()

    # Create the worker the handle calulating the true md5 for conflicts
    for i in range(numThreads):
        tRealHash = RealHashCalculator(realHashQueue, verbose)
        tRealHash.setDaemon(True)
        tRealHash.start()

    # Start finding files to process
    findFilesToProcess(dir, fileQueue, recursive)

    fileQueue.join()
    digestQueue.join()
    realHashQueue.join()

    # If we have enabled delete, then delete items that are in the same directory
    if (delete):
        autoDeleteDuplicates(allDigests, verbose)

    if (outFile != 0):
        # Directly write to XML
        writeToXml(outFile, allDigests)

# Handle a arguments, then call main
parser = argparse.ArgumentParser();
parser.add_argument("-s", "--search", help="The directory to search for duplicates.")
parser.add_argument("-v", "--verbose", action='store_true', default=False, help="Verbose output")
parser.add_argument("-r", "--recursive", action='store_true', help="If supplied, the program will search all child directories of dir for images.")
parser.add_argument("-t", "--threads", type=int, default=1, help="The number of main analysis threads to use.")
parser.add_argument("-o", "--out", help="If supplied, generate an XML report at the specified path.")
parser.add_argument("-d", "--delete", action='store_true', help="If supplied, the program will automatically delete exact duplicates that are in the same folder. Deleted files are permanently deleted and cannot be recovered.")
args = parser.parse_args();

dir = os.path.abspath(args.search)
outPath = os.path.abspath(args.out)

allDigests = dict()
outFile = 0
if (args.out):
    outFile = open(outPath, 'w', encoding="utf-8")

main(dir, allDigests, args.recursive, args.verbose, args.delete, args.threads, outFile)
