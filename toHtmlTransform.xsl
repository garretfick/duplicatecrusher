﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
<xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>
<xsl:template match="/">

  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <title>Duplicates Report</title>
    </head>
    <body>
      <xsl:for-each select="duplicateReport/duplicateSet">
        <h1>Duplicate</h1>
        <ul>
          <xsl:for-each select="./*">
            <li>
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="@path"/>
                </xsl:attribute>
                <xsl:value-of select="@path"/>
              </a>
            </li>
          </xsl:for-each>
        </ul>
      </xsl:for-each>
    </body>
  </html>
</xsl:template>
</xsl:stylesheet>
