# README #

DuplicateCrusher is a simple Python script for finding exact duplicate images. It calculates an MD5 sum based on the RGB pixels of the image (ignoring EXIF and other data). DuplicateCrusher can produce a report identifying duplicate images, and can optionally automatically delete extra copies. It does not help find similar images.

I wrote it for myself to solve my particular needs and am making it freely available.

## How do I get set up? ##

DuplicateCrusher is a simple Python script. To use DuplicateCrusher, you need to install Python and its dependencies, then run the script.

### Install dependencies ###
You need to install the following:

* [Python 3.4](http://python.org)
* Pillow ([Windows](http://www.lfd.uci.edu/~gohlke/pythonlibs/))

### Run DuplicateCrusher ###

Once you have installed the above, download the script and run it from the command line. Use the -h switch to get a description of how to use DuplicateCrusher.

    python crushDuplicates.py -h

**WARNING** DuplicateCrusher can delete photos. Unless you are completely certain of what you are doing, you should create a backup of your photos and verify the result before deleting the backup. 

The following command produces a report identifying found duplicates.

    python crushDuplicates.py -r -o C:\Users\USER NAME\Documents\report.xml -s C:\Users\USER NAME\Pictures 

The parameters have the following meanings

    -r      tells the script to seach for files recursively.

    -o C:\Users\USER NAME\Documents\report.xml is where to create the report.

    -s C:\Users\USER NAME\Pictures is the directory to search for pictures

The following command finds and automatically deletes extra copies.

    python crushDuplicates.py -r -d -s C:\Users\USER NAME\Pictures 

The parameters have the following meanings

    -r      tells the script to seach for files recursively

    -d      tells the script to automatically delete extra copies.

    -s C:\Users\USER NAME\Pictures is the directory to search for pictures

DuplicateCrusher can use multiple threads and you will notice significant performance boost by enabling multiple analysis threads.

    -t 4    tells the script to use 4 main analysis threads.

## Contribution ##

I welcome your contributions to improve the content. The best ways are:

* Create an issue asking to contribute
* Send me a message at [Fick's Workshop](http://ficksworkshop.com/contact)

## Donation ##

If you really like DuplicateCrusher or just want to help support me. You can donate to me via Paypal.

## License ##

The MIT License (MIT)

Copyright (c) 2015 Garret Fick and Fick's Workshop

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.